# Sudoku
Simple sudoku solver and generator written in python.

## Usage
### Generate sudoku
```python
from sudoku.generator import Generator, Complexity
from sudoku.solver import BackTrackingSolver
from sudoku.formatter import TextFormatter

generator = Generator(complexity=Complexity.NORMAL)

board = generator.generate()

print(TextFormatter(board).format())
+---+---+---+---+---+---+---+---+---+
| 3   1   9 | 2         | 7         |
| 4   5     |           |         8 |
| 8   6   2 | 5   1   7 |           |
+---+---+---+---+---+---+---+---+---+
|           | 8       9 | 5   7   4 |
| 5         | 1   3   6 |         2 |
|     9   8 |     4   5 | 6       1 |
+---+---+---+---+---+---+---+---+---+
| 6         |         2 | 3         |
|     4   5 | 3   6     |     1     |
|         3 |     7   1 |           |
+---+---+---+---+---+---+---+---+---+
```

### Solve sudoku
```python
from sudoku.generator import Generator, Complexity
from sudoku.solver import BackTrackingSolver
from sudoku.formatter import TextFormatter

generator = Generator(Complexity.NORMAL)

board = generator.generate()

formatter = TextFormatter(board)
print(formatter.format())
+---+---+---+---+---+---+---+---+---+
|         5 |         9 |     3   1 |
| 3   2   7 | 1   6     | 8       4 |
| 9   1     |           |     6     |
+---+---+---+---+---+---+---+---+---+
|         3 | 7   5     | 4   2     |
| 4       9 |     2     |         6 |
| 2         | 6   9     | 3   8     |
+---+---+---+---+---+---+---+---+---+
| 7       8 |     3     |           |
|         2 | 4       8 | 6   7   3 |
| 1   3     |           | 5   4     |
+---+---+---+---+---+---+---+---+---+

BackTrackingSolver(board).solve()
print(formatter.format())
+---+---+---+---+---+---+---+---+---+
| 6   8   5 | 2   4   9 | 7   3   1 |
| 3   2   7 | 1   6   5 | 8   9   4 |
| 9   1   4 | 3   8   7 | 2   6   5 |
+---+---+---+---+---+---+---+---+---+
| 8   6   3 | 7   5   1 | 4   2   9 |
| 4   7   9 | 8   2   3 | 1   5   6 |
| 2   5   1 | 6   9   4 | 3   8   7 |
+---+---+---+---+---+---+---+---+---+
| 7   4   8 | 5   3   6 | 9   1   2 |
| 5   9   2 | 4   1   8 | 6   7   3 |
| 1   3   6 | 9   7   2 | 5   4   8 |
+---+---+---+---+---+---+---+---+---+
```

### Serialize into json and import board from json
```python
from sudoku.generator import Generator, Complexity
from sudoku.solver import BackTrackingSolver
from sudoku.formatter import TextFormatter
from sudoku.serializer import JsonSerializer

generator = Generator(Complexity.SIMPLE)

board = generator.generate()
board_json = JsonSerializer().encode(board)
print(board_json)
{"board": {"cells": [[{"value": 3, "frozen": true}, {"value": 1, "frozen": true}, {"value": 9, "frozen": true}, {"value": 2, "frozen": true}, {"value": 8, "frozen": false}, {"value": 4, "frozen": false}, {"value": 7, "frozen": true}, {"value": 5, "frozen": false}, {"value": 6, "frozen": false}], [{"value": 4, "frozen": true}, {"value": 5, "frozen": true}, {"value": 7, "frozen": false}, {"value": 6, "frozen": false}, {"value": 9, "frozen": false}, {"value": 3, "frozen": false}, {"value": 1, "frozen": false}, {"value": 2, "frozen": false}, {"value": 8, "frozen": true}], [{"value": 8, "frozen": true}, {"value": 6, "frozen": true}, {"value": 2, "frozen": true}, {"value": 5, "frozen": true}, {"value": 1, "frozen": true}, {"value": 7, "frozen": true}, {"value": 4, "frozen": false}, {"value": 9, "frozen": false}, {"value": 3, "frozen": false}], [{"value": 1, "frozen": false}, {"value": 3, "frozen": false}, {"value": 6, "frozen": false}, {"value": 8, "frozen": true}, {"value": 2, "frozen": false}, {"value": 9, "frozen": true}, {"value": 5, "frozen": true}, {"value": 7, "frozen": true}, {"value": 4, "frozen": true}], [{"value": 5, "frozen": true}, {"value": 7, "frozen": false}, {"value": 4, "frozen": false}, {"value": 1, "frozen": true}, {"value": 3, "frozen": true}, {"value": 6, "frozen": true}, {"value": 9, "frozen": false}, {"value": 8, "frozen": false}, {"value": 2, "frozen": true}], [{"value": 2, "frozen": false}, {"value": 9, "frozen": true}, {"value": 8, "frozen": true}, {"value": 7, "frozen": false}, {"value": 4, "frozen": true}, {"value": 5, "frozen": true}, {"value": 6, "frozen": true}, {"value": 3, "frozen": false}, {"value": 1, "frozen": true}], [{"value": 6, "frozen": true}, {"value": 8, "frozen": false}, {"value": 1, "frozen": false}, {"value": 9, "frozen": false}, {"value": 5, "frozen": false}, {"value": 2, "frozen": true}, {"value": 3, "frozen": true}, {"value": 4, "frozen": false}, {"value": 7, "frozen": false}], [{"value": 7, "frozen": false}, {"value": 4, "frozen": true}, {"value": 5, "frozen": true}, {"value": 3, "frozen": true}, {"value": 6, "frozen": true}, {"value": 8, "frozen": false}, {"value": 2, "frozen": false}, {"value": 1, "frozen": true}, {"value": 9, "frozen": false}], [{"value": 9, "frozen": false}, {"value": 2, "frozen": false}, {"value": 3, "frozen": true}, {"value": 4, "frozen": false}, {"value": 7, "frozen": true}, {"value": 1, "frozen": true}, {"value": 8, "frozen": false}, {"value": 6, "frozen": false}, {"value": 5, "frozen": false}]]}}

decoded_board = JsonSerializer().decode(board_json)
print(TextFormatter(decoded_board).format())
+---+---+---+---+---+---+---+---+---+
| 3   1   9 | 2   8   4 | 7   5   6 |
| 4   5   7 | 6   9   3 | 1   2   8 |
| 8   6   2 | 5   1   7 | 4   9   3 |
+---+---+---+---+---+---+---+---+---+
| 1   3   6 | 8   2   9 | 5   7   4 |
| 5   7   4 | 1   3   6 | 9   8   2 |
| 2   9   8 | 7   4   5 | 6   3   1 |
+---+---+---+---+---+---+---+---+---+
| 6   8   1 | 9   5   2 | 3   4   7 |
| 7   4   5 | 3   6   8 | 2   1   9 |
| 9   2   3 | 4   7   1 | 8   6   5 |
+---+---+---+---+---+---+---+---+---+
```
