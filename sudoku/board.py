from dataclasses import dataclass
from typing import Any, Generator, List, Tuple

from .errors import SudokuError

EMPTY_VALUE = 0
VALID_VALUES = {1, 2, 3, 4, 5, 6, 7, 8, 9}

BOARD_WIDTH = 9
BOARD_HEIGHT = 9
BOARD_CELLS_COUNT = BOARD_HEIGHT * BOARD_WIDTH
SQUARE_SIZE = 3
BOARD_SQUARES = (BOARD_WIDTH // SQUARE_SIZE) * (BOARD_HEIGHT // SQUARE_SIZE)

ListOfCellValues = List[List[int]]


class InvalidCellAddressError(SudokuError):
    """Raised in case of invalid cell address."""

    row = None
    col = None
    reason: str

    def __init__(self, row, col, reason: str):
        """Initialize error instance.

        Args:
            row (int): row number
            col (int): col number
            error_message (str): error message which provide error details
        """
        super().__init__()
        self.row = row
        self.col = col
        self.reason = reason


class InvalidCellValueError(SudokuError):
    """Raised in case of attemp to set invalid cell value."""

    error_value: Any = None
    reason: str

    def __init__(self, error_value: Any, reason: str) -> None:
        super().__init__()
        self.error_value = error_value
        self.reason = reason


class FrozenCellError(SudokuError):
    """Raised on attempt to modify or clear a frozen cell value."""

    row: int = 0
    col: int = 0

    def __init__(self, row: int, col: int):
        super().__init__()

        self.row = row
        self.col = col


@dataclass
class Cell:
    value: int = EMPTY_VALUE
    frozen: bool = False

    def is_empty(self) -> bool:
        return self.value == EMPTY_VALUE

    def is_frozen(self):
        return self.frozen

    def set_value(self, value):
        self._validate_value(value)
        self._set_value(value)

    @classmethod
    def _validate_value(cls, value: int):
        cls._validate_value_type(value)
        cls._validate_value_value(value)

    def _set_value(self, value: int):
        if self.is_frozen():
            raise FrozenCellError(0, 0)  # TODO: this does not look right.

        self.value = value

    @staticmethod
    def _validate_value_type(value: int):
        if type(value) != int:
            raise InvalidCellValueError(value, "Cell value must be integer")

    @staticmethod
    def _validate_value_value(value: int):
        if value != EMPTY_VALUE and value not in VALID_VALUES:
            raise InvalidCellValueError(value, f"Value must be in {VALID_VALUES.union({EMPTY_VALUE})}")

    def get_value(self) -> int:
        return self.value

    def freeze(self):
        self.frozen = True

    def clear_value(self):
        self._set_value(EMPTY_VALUE)


class Coordinates:
    @classmethod
    def validate_row_and_col(cls, row: int, col: int):
        try:
            cls._validate_index_type(row)
            cls._validate_index_type(col)
        except TypeError:
            raise InvalidCellAddressError(row, col, "Coordinates must be integers")

        try:
            cls._validate_index_is_in_range(row, 0, BOARD_HEIGHT)
        except ValueError as err:
            raise InvalidCellAddressError(row, col, f"Row must be between 0 and {BOARD_HEIGHT - 1}")

        try:
            cls._validate_index_is_in_range(col, 0, BOARD_WIDTH)
        except ValueError as err:
            raise InvalidCellAddressError(row, col, f"Col must be between 0 and {BOARD_WIDTH - 1}")

    @staticmethod
    def _validate_index_type(idx: int):
        if type(idx) != int:
            raise TypeError(type(idx))

    @staticmethod
    def _validate_index_is_in_range(idx: int, start, stop):
        if idx not in range(start, stop):
            raise ValueError(f"value must be between {start} and {stop - 1}")

    @classmethod
    def get_square_id_from_row_col(cls, row: int, col: int) -> int:
        cls.validate_row_and_col(row, col)

        return (row // SQUARE_SIZE) * 3 + (col // SQUARE_SIZE)

    @classmethod
    def get_square_top_left_corner(cls, square: int) -> Tuple[int, int]:
        """Convert square idx to row and col of the top left corner of the square.

        Args:
            square ([int]): square id 0..9

        Returns:
            Tuple[int, int]: (row, col) top left cell of the square.
        """
        row = (square // SQUARE_SIZE) * SQUARE_SIZE
        col = (square % SQUARE_SIZE) * SQUARE_SIZE
        return (row, col)


class Board:

    _cells: List[List[Cell]] = []
    _coordinates: Coordinates

    def __init__(self, cells: List[List[Cell]]) -> None:
        self._cells = cells
        self._coordinates = Coordinates()

    @classmethod
    def create_empty(cls) -> "Board":
        return Board([[Cell(value=EMPTY_VALUE, frozen=False) for _ in range(BOARD_WIDTH)] for _ in range(BOARD_HEIGHT)])

    @classmethod
    def create_from_cell_values(cls, values: ListOfCellValues) -> "Board":
        sudoku_board = cls.create_empty()
        for row, row_cells in enumerate(sudoku_board.rows()):
            for col, cell in enumerate(row_cells):
                future_cell_value = values[row][col]
                cell.set_value(future_cell_value)
                if not cell.is_empty():
                    cell.freeze()

        return sudoku_board

    def get_cell(self, row: int, col: int) -> Cell:
        self._coordinates.validate_row_and_col(row, col)
        return self._get_cell(row, col)

    def _get_cell(self, row: int, col: int) -> Cell:
        return self._cells[row][col]

    def is_solved(self) -> bool:
        """Returns True if the board has no empty fields."""

        def is_not_empty(cell: Cell) -> bool:
            return not cell.is_empty()

        all_cells = (cell for row_cells in self.rows() for cell in row_cells)

        return all(is_not_empty(cell) for cell in all_cells)

    def rows(self) -> Generator[List[Cell], None, None]:
        """Generator which returns board's rows.

        Yields:
            Generator[Tuple[int, List[Cell]], None, None]: row id and list of cells which belong to that row.
        """
        for row in range(BOARD_HEIGHT):
            yield self._get_row_cells(row)

    def _get_row_cells(self, row: int) -> List[Cell]:
        return [self._get_cell(row, col) for col in range(BOARD_WIDTH)]

    def cols(self) -> Generator[List[Cell], None, None]:
        """Generator that yields cells of board columns.

        Yields:
            Generator[Tuple[int, List[Cell]], None, None]: column id and list of cells which belong to that column.
        """
        for col in range(BOARD_WIDTH):
            yield self._get_col_cells(col)

    def _get_col_cells(self, col: int) -> List[Cell]:
        return [self._get_cell(row, col) for row in range(BOARD_HEIGHT)]

    def squares(self) -> Generator[List[Cell], None, None]:
        """Generator which yields board squares.

        Yields:
            Generator[Tuple[int, List[Cell]], None, None]: square id and list of cells which belong to that square.
        """
        for square in range(BOARD_SQUARES):
            yield self._get_square_cells(square)

    def _get_square_cells(self, square: int) -> List[Cell]:
        (base_row, base_col) = self._coordinates.get_square_top_left_corner(square)
        return [
            self._get_cell(base_row + row_delta, base_col + col_delta)
            for row_delta in range(SQUARE_SIZE)
            for col_delta in range(SQUARE_SIZE)
        ]

    def get_row(self, row: int) -> List[Cell]:
        return self._get_row_cells(row)

    def get_col(self, col: int) -> List[Cell]:
        return self._get_col_cells(col)

    def get_square(self, square: int) -> List[Cell]:
        return self._get_square_cells(square)
