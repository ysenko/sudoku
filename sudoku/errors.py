from typing import TYPE_CHECKING


# The following import causes cyclic import error if done in a regular way, however it's only required for type checking.
if TYPE_CHECKING:
    from .validator import ValidatorType
    from board import Cell


class SudokuError(Exception):
    """Base error.

    All other errors implements it.
    """


class ValidationError(SudokuError):
    validation_type: "ValidatorType"
    cells: "list[Cell]"

    def __init__(self, validation_type: "ValidatorType", cells: list["Cell"]):
        self.validation_type = validation_type
        self.cells = cells
