import abc
from typing import Protocol

from .board import BOARD_HEIGHT, SQUARE_SIZE, Board, Cell


class FormatterProtocol(Protocol):

    board: Board

    def _initialize_board(self, sudoku_board: Board):
        self.board = sudoku_board

    @abc.abstractmethod
    def format(self):
        pass


class TextFormatter(FormatterProtocol):

    ROW_SEP = "+---+---+---+---+---+---+---+---+---+"
    COL_SEP = "|"
    ROW_TMPL = "| {}   {}   {} | {}   {}   {} | {}   {}   {} |"
    _lines: list[str]
    EMPTY_VALUE_STR = " "
    LINE_BREAK = "\n"

    def __init__(self, sudoku_board: Board):
        self._initialize_board(sudoku_board)
        self._initialize_formatter()

    def format(self) -> str:
        """Produce text representation of the boar suitable for terminal, text files, etc.

        Returns:
            str: board in text format.
        """
        self._initialize_formatter()
        for row_idx, row in enumerate(self.board.rows()):
            self._add_row_top_sep(row_idx)
            self._add_row(row)
            self._add_row_bottom_sep(row_idx)

        return self._create_board_str_repr()

    def _initialize_formatter(self):
        self._lines = []

    def _create_board_str_repr(self) -> str:
        return self.LINE_BREAK.join(self._lines)

    def _add_row(self, row: list[Cell]):
        row_string = self._row_to_str(row)
        self._lines.append(row_string)

    def _row_to_str(self, row: list[Cell]) -> str:
        return self.ROW_TMPL.format(*[self._cell_to_str(cell) for cell in row])

    def _cell_to_str(self, cell: Cell):
        if cell.is_empty():
            return self.EMPTY_VALUE_STR
        return str(cell.get_value())

    def _add_row_top_sep(self, row_idx: int):
        if self._row_top_sep_required(row_idx):
            self._add_row_sep()

    def _row_top_sep_required(self, row_idx: int) -> bool:
        is_first_row_of_the_square = lambda: row_idx % SQUARE_SIZE == 0
        return is_first_row_of_the_square()

    def _add_row_bottom_sep(self, row_idx: int):
        if self._row_bottom_sep_required(row_idx):
            self._add_row_sep()

    def _row_bottom_sep_required(self, row_idx: int) -> bool:
        is_last_row = lambda: row_idx == (BOARD_HEIGHT - 1)
        return is_last_row()

    def _add_row_sep(self):
        self._lines.append(self.ROW_SEP)
