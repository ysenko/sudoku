import random
from enum import Enum
from typing import Optional, Type

from ..solver import BackTrackingSolver, SolverProtocol
from ..board import Board, BOARD_WIDTH, BOARD_HEIGHT


class Complexity(Enum):
    SIMPLE = 1
    NORMAL = 2
    HARD = 3


class Generator:

    solver_cls: Type[SolverProtocol]
    _generated_board: Board
    _complexity: Complexity

    EMPTY_CELLS_PER_COMPLEXITY: dict[Complexity, int] = {
        Complexity.SIMPLE: 20,
        Complexity.NORMAL: 40,
        Complexity.HARD: 70,
    }

    def __init__(self, complexity: Complexity):
        self.solver_cls = BackTrackingSolver
        self._complexity = complexity

    def generate(self, complexity: Optional[Complexity] = None) -> Board:
        self._set_complexity(complexity)
        self._generate_solved_board()
        self._remove_values()
        self.freeze_board()
        return self._generated_board

    def _set_complexity(self, complexity: Optional[Complexity] = None):
        if complexity is None:
            complexity = Complexity.NORMAL

        self._complexity = complexity

    def _generate_solved_board(self):
        board = Board.create_empty()
        solver = self.solver_cls.create(board)
        solver.solve()
        solver.validate_board()
        self._generated_board = board

    def _get_removed_values_count(self) -> int:
        return self.EMPTY_CELLS_PER_COMPLEXITY[self._complexity]

    def _remove_values(self):
        cells_to_clear = set()
        removed_values_count = self._get_removed_values_count()
        while len(cells_to_clear) < removed_values_count:
            cells_to_clear.add(self._get_random_cell_address())

        for cell in cells_to_clear:
            self._generated_board.get_cell(*cell).clear_value()

    @classmethod
    def _get_random_cell_address(cls) -> tuple[int, int]:
        row_indexes = list(range(BOARD_HEIGHT))
        col_indexes = list(range(BOARD_WIDTH))
        return (random.choice(row_indexes), random.choice(col_indexes))

    def freeze_board(self):
        for row in range(BOARD_HEIGHT):
            for col in range(BOARD_WIDTH):
                self._freeze_cell(row, col)

    def _freeze_cell(self, row: int, col: int):
        cell_to_freeze = self._generated_board.get_cell(row, col)
        if not cell_to_freeze.is_empty():
            cell_to_freeze.freeze()
