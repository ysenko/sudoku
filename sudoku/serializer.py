import abc
import json
from typing import Any, Protocol

from sudoku.board import Board


class SerializerProtocol(Protocol):
    @abc.abstractmethod
    def encode(self, board: Board) -> Any:
        pass

    @abc.abstractmethod
    def decode(self, payload: Any) -> Board:
        pass


class JsonSerializer(SerializerProtocol):
    def encode(self, sudoku_board: Board) -> str:
        return json.dumps(
            {
                "board": {
                    "cells": [
                        [{"value": cell.get_value(), "frozen": cell.is_frozen()} for cell in row]
                        for row in sudoku_board.rows()
                    ]
                }
            }
        )

    def decode(self, board_json: str) -> Board:
        board_dict = json.loads(board_json)

        res = Board.create_empty()

        for row_idx, row in enumerate(board_dict["board"]["cells"]):
            for col_idx, cell_dict in enumerate(row):
                cell = res.get_cell(row_idx, col_idx)
                cell.set_value(cell_dict["value"])
                if cell_dict["frozen"]:
                    cell.freeze()

        return res
