import abc

from typing import List, Protocol, Type

from sudoku import board
from sudoku.validator import ValidatorProtocol, ColValidator, ColValidator, SquareValidator


class SolverProtocol(Protocol):
    """Sudoku solver abstract class.

    Other solvers are responsible for implementing it.
    """

    _board: board.Board
    _validator_classes: list[Type[ValidatorProtocol]] = [ColValidator, ColValidator, SquareValidator]
    _validators: List[ValidatorProtocol]
    _coordinates: board.Coordinates

    def __init__(self, sudoku_board: board.Board):
        self._initialize_solver(sudoku_board)

    def _initialize_solver(self, sudoku_board: board.Board):
        self._attach_board(sudoku_board)
        self._initialize_validators()
        self._initialize_coordinates()

    def _attach_board(self, sudoku_board: board.Board):
        self._board = sudoku_board

    def _initialize_coordinates(self):
        self._coordinates = board.Coordinates()

    @classmethod
    def create(cls, sudoku_board: board.Board):
        """Create and return solver instance.

        Returns:
            SudokuSolver: initialized instance for the class.
        """
        return cls(sudoku_board)

    def _initialize_validators(self):
        self._validators = [validator_cls(self._board) for validator_cls in self._validator_classes]

    def validate_board(self):
        """Validate board against set of validators.

        Raises:
            ValidationError in case of any problems detected.
        """
        for validator in self._validators:
            validator.validate()

    @abc.abstractmethod
    def solve(self) -> None:
        """Solve sudoku board.

        Raises:
            UnsolvableError: in case if board cannot be solved.
        """
        pass
