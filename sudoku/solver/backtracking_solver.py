import logging
import random
from typing import Generator, Iterator

from .abstract_solver import SolverProtocol
from sudoku import board
from .errors import SolverError, UnsolvableError


logger = logging.getLogger(__file__)


class _NoValidOptionsError(SolverError):
    """Raised by the solver if cell has no valid options."""

    row: int
    col: int

    def __init__(self, row: int, col: int):
        self.row = row
        self.col = col


class NothingToRollbackError(SolverError):
    """Indicates that it's not possible to rollback since there is nothing to rollback."""


class BackTracker:

    _board: board.Board
    solved_cell_history: list[tuple[int, int]]
    cell_used_values: dict[tuple[int, int], set[int]]

    def __init__(self, sudoku_board: board.Board):
        self._board = sudoku_board
        self.solved_cell_history = []
        self.cell_used_values = {}

    def track_solved_cell(self, row: int, col: int, value: int):
        cell = self._board.get_cell(row, col)
        cell.set_value(value)
        self._track_cell_value(row, col, value)
        self._track_solved_cell(row, col)

    def _track_solved_cell(self, row, col):
        self.solved_cell_history.append((row, col))

    def _track_cell_value(self, row: int, col: int, value: int):
        used_values = self.cell_used_values.get((row, col), set())
        used_values.add(value)
        self.cell_used_values[(row, col)] = used_values

    def get_used_cell_values(self, row: int, col: int) -> set[int]:
        return self.cell_used_values.get((row, col), set())

    def rollback(self):
        row, col = self._rollback_solved_cell()
        self._cleanup_cell_values(row, col)

    def _rollback_solved_cell(self) -> tuple[int, int]:
        if self.solved_cell_history:
            row, col = self.solved_cell_history.pop()
            cell = self._board.get_cell(row, col)
            cell.clear_value()
            return (row, col)
        else:
            raise NothingToRollbackError()

    def _cleanup_cell_values(self, base_row: int, base_col: int):
        for row, col in self._get_cells_to_cleanup(base_row, base_col):
            cell = self._board.get_cell(row, col)
            cell.clear_value()
            self._cleanup_cell_used_values(row, col)

    def _get_cells_to_cleanup(self, base_row: int, base_col: int) -> Generator[tuple[int, int], None, None]:
        # TODO: the logic is complex and not obvious. Consider rewriting.
        def is_not_frozen(row: int, col: int) -> bool:
            cell = self._board.get_cell(row, col)
            return not cell.is_frozen()

        def is_larger_than_base_cell(row: int, col: int) -> bool:
            if row > base_row:
                return True
            elif row == base_row:
                return col > base_col
            return False

        for row in range(base_row, board.BOARD_HEIGHT):
            for col in range(board.BOARD_WIDTH):
                if is_not_frozen(row, col) and is_larger_than_base_cell(row, col):
                    yield (row, col)

    def _cleanup_cell_used_values(self, row: int, col: int):
        cell = (row, col)
        if cell in self.cell_used_values:
            self.cell_used_values.pop(cell)


class BackTrackingSolver(SolverProtocol):
    """Sudoku back tracking solver.

    For each cell it identifies a list of possible values and choose the first one. Once it hit the cell without valid options it rolls back the most recent decision and tries the next value from the list on available options.
    """

    _blacklisted_cell_values: dict[tuple[int, int], set[int]]
    _solved_cells_log = list[tuple[int, int]]

    _tracker: BackTracker
    _coordinates: board.Coordinates

    def __init__(self, sudoku_board: board.Board):
        self._initialize_solver(sudoku_board)
        self._initialize_backtracker()

    def _initialize_solver(self, sudoku_board: board.Board):
        super()._initialize_solver(sudoku_board)
        self._blacklisted_cell_values = {}
        self._solved_cells_log = []

    def _initialize_backtracker(self):
        self._tracker = BackTracker(self._board)

    def _get_row_non_empty_cells(self, row: int) -> Iterator[int]:
        return self._cells_to_values(self._filter_empty_cells(self._board.get_row(row)))

    def _get_col_non_empty_cells(self, col: int) -> Iterator[int]:
        return self._cells_to_values(self._filter_empty_cells(self._board.get_col(col)))

    def _get_square_non_empty_cells(self, square: int) -> Iterator[int]:
        return self._cells_to_values(self._filter_empty_cells(self._board.get_square(square)))

    @staticmethod
    def _cells_to_values(cells: list[board.Cell]) -> Iterator[int]:
        return (cell.get_value() for cell in cells)

    @staticmethod
    def _filter_empty_cells(cells: list[board.Cell]) -> list[board.Cell]:
        return [cell for cell in cells if not cell.is_empty()]

    def get_cell_possible_values(self, row: int, col: int) -> list[int]:
        cell = self._board._get_cell(row, col)
        if not cell.is_empty():
            return [cell.get_value()]

        square = self._coordinates.get_square_id_from_row_col(row, col)

        row_non_empty_values = self._get_row_non_empty_cells(row)
        col_non_empty_values = self._get_col_non_empty_cells(col)
        square_non_empty_values = self._get_square_non_empty_cells(square)

        already_tried_values = self._tracker.get_used_cell_values(row, col)

        all_used_values = (
            set(row_non_empty_values) | set(col_non_empty_values) | set(square_non_empty_values) | already_tried_values
        )

        possible_values = board.VALID_VALUES - all_used_values

        return list(possible_values)

    def _solve_cell(self, row: int, col: int):
        cell_options = self.get_cell_possible_values(row, col)
        if cell_options:
            new_value = random.choice(cell_options)
            logger.debug("Cell (%d, %d) <= %d. Options: %s", row, col, new_value, str(cell_options))
            self._tracker.track_solved_cell(row, col, new_value)
        else:
            logger.debug("Cell (%d, %d) - No valid options", row, col)
            raise _NoValidOptionsError(row, col)

    def _rollback_recently_solved_cell(self):
        try:
            self._tracker.rollback()
        except NothingToRollbackError:
            raise UnsolvableError

    def _get_next_unsolved_cell(self,) -> Generator[tuple[int, int], None, None]:
        """Generate coordinates of next unsolved cell.

        Yields all unsolved cell coordinates one by one starting from the (0, 0) all the way down to (8, 8).

        Yields:
            Generator[Tuple[int, int], None, None]: (row, col) of the next unsolved cell.
        """
        for row in range(board.BOARD_HEIGHT):
            for col in range(board.BOARD_WIDTH):
                cell = self._board.get_cell(row, col)
                if cell.is_empty():
                    yield (row, col)

    def solve(self):
        while not self._board.is_solved():
            try:
                self._attempt_solve()
            except _NoValidOptionsError as err:
                self._rollback_recently_solved_cell()

    def _attempt_solve(self):
        for row, col in self._get_next_unsolved_cell():
            self._solve_cell(row, col)
