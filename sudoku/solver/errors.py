from sudoku.errors import SudokuError


class SolverError(SudokuError):
    """Base class for all solver errors."""


class UnsolvableError(SolverError):
    """Raised when the board cannot be solved."""
