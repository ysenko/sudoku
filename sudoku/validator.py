import abc
from enum import Enum
from typing import Optional, Protocol

from sudoku import board
from sudoku.errors import ValidationError


class ValidatorType(Enum):
    ROW = 1
    COL = 2
    SQUARE = 3


class ValidatorProtocol(Protocol):
    """Abstract validator class which other validators need to implement."""

    _sudoku_board: Optional[board.Board]
    _validation_type: ValidatorType

    def _initialize(self, sudoku_board: board.Board):
        self._sudoku_board = sudoku_board

    @abc.abstractmethod
    def validate(self):
        pass

    def _validate_values(self, cells: list[board.Cell]):
        """Validate a list of values i.e. row, col or square.

        Args:
            values (List[board.Cells]): cells to validate

        Raises:
            ValidationError: in case if `cells` contains duplicates of non-empty values.
        """
        non_empty_cell_values = [cell.get_value() for cell in cells if not cell.is_empty()]
        uniq_non_empty_values = set(non_empty_cell_values)

        if len(non_empty_cell_values) != len(uniq_non_empty_values):
            raise ValidationError(validation_type=self._validation_type, cells=cells)


class BaseValidator(ValidatorProtocol):
    def __init__(self, sudoku_board: board.Board):
        self._initialize(sudoku_board)


class RowValidator(BaseValidator):
    """Validates board rows to make sure that they does not contain duplicates."""

    _validation_type = ValidatorType.ROW

    def validate(self):
        for row in self._sudoku_board.rows():
            print(row)
            self._validate_values(row)


class ColValidator(BaseValidator):

    _validation_type = ValidatorType.COL

    def __init__(self, sudoku_board: board.Board):
        self._initialize(sudoku_board)

    def validate(self):
        for col in self._sudoku_board.cols():
            self._validate_values(col)


class SquareValidator(BaseValidator):
    def __init__(self, sudoku_board: board.Board):
        self._initialize(sudoku_board)

    _validation_type = ValidatorType.SQUARE

    def validate(self):
        for square in self._sudoku_board.squares():
            self._validate_values(square)
