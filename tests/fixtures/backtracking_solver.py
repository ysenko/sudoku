import pytest

from sudoku.board import Board

from .board import empty_test_board
from sudoku.solver.backtracking_solver import BackTracker

TrackerAndBoard = tuple[BackTracker, Board]


@pytest.fixture
def back_tracker(empty_test_board: Board) -> TrackerAndBoard:
    return BackTracker(empty_test_board), empty_test_board
