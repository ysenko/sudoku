import py
import pytest

from sudoku.board import Board, EMPTY_VALUE as E, ListOfCellValues


NOT_EMPTY_BOARD_EXPECTED_VALUES: ListOfCellValues = [
    [E, E, 7, 4, 9, 1, 6, E, 5],
    [2, E, E, E, 6, E, 3, E, 9],
    [E, E, E, E, E, 7, E, 1, E],
    [E, 5, 8, 6, E, E, E, E, 4],
    [E, E, 3, E, E, E, E, 9, E],
    [E, E, 6, 2, E, E, 1, 8, 7],
    [9, E, 4, E, 7, E, E, E, 2],
    [6, 7, E, 8, 3, E, E, E, E],
    [8, 1, E, E, 4, 5, E, E, E],
]


@pytest.fixture
def empty_test_board() -> Board:
    return Board.create_empty()


@pytest.fixture
def test_board(values=None) -> Board:
    if values is None:
        values = NOT_EMPTY_BOARD_EXPECTED_VALUES

    return Board.create_from_cell_values(values)
