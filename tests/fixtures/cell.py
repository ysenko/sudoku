import pytest

from sudoku.board import Cell

EXPECTED_CELL_VALUE = 4


@pytest.fixture
def empty_cell() -> Cell:
    return Cell()


@pytest.fixture
def frozen_cell() -> Cell:
    return Cell(EXPECTED_CELL_VALUE, True)


@pytest.fixture
def not_empty_cell() -> Cell:
    return Cell(EXPECTED_CELL_VALUE)
