import pytest

from .board import test_board, NOT_EMPTY_BOARD_EXPECTED_VALUES
from sudoku.board import Board
from sudoku.formatter import TextFormatter


@pytest.fixture
def text_formatter(test_board: Board) -> TextFormatter:
    return TextFormatter(test_board)
