import pytest

from sudoku.solver.backtracking_solver import BackTrackingSolver, NothingToRollbackError

from .fixtures.board import empty_test_board, test_board
from .fixtures.backtracking_solver import TrackerAndBoard, back_tracker
from sudoku.board import EMPTY_VALUE, Board, BOARD_HEIGHT, BOARD_WIDTH, VALID_VALUES


class TestBackTracker:
    def test_given_tracked_cell_when_getting_cell_value_then_it_must_be_the_same_as_tracked(
        self, back_tracker: TrackerAndBoard
    ):
        tracker, board = back_tracker
        cell_coordinates = (1, 2)
        expected_cell_value = 3
        tracker.track_solved_cell(*cell_coordinates, expected_cell_value)

        actual_cell_value = board.get_cell(*cell_coordinates).get_value()
        assert expected_cell_value == actual_cell_value

    def test_given_tracked_cell_when_getting_cell_used_values_then_tracked_value_must_be_in_the_list(
        self, back_tracker: TrackerAndBoard
    ):
        tracker, _ = back_tracker
        cell_coordinates = (1, 2)
        expected_cell_value = 3
        tracker.track_solved_cell(*cell_coordinates, expected_cell_value)

        used_cell_values = tracker.get_used_cell_values(*cell_coordinates)

        assert {expected_cell_value} == used_cell_values

    def test_given_tracked_value_when_perform_rollback_then_cell_value_must_be_cleared(
        self, back_tracker: TrackerAndBoard
    ):
        tracker, board = back_tracker
        cell_coordinates = (1, 2)
        tracked_value = 3
        tracker.track_solved_cell(*cell_coordinates, tracked_value)

        tracker.rollback()

        assert board.get_cell(*cell_coordinates).is_empty()

    def test_given_tracked_value_when_performing_rollback_then_tracked_value_must_remain_in_the_list(
        self, back_tracker: TrackerAndBoard
    ):
        tracker, _ = back_tracker
        cell_coordinates = (1, 2)
        tracked_value = 3
        expected_used_values = {tracked_value}
        tracker.track_solved_cell(*cell_coordinates, tracked_value)

        tracker.rollback()

        actual_used_values = tracker.get_used_cell_values(*cell_coordinates)
        assert expected_used_values == actual_used_values

    def test_given_empty_cell_with_with_non_empty_used_values_when_performing_rollback_then_used_values_must_be_cleared(
        self, back_tracker: TrackerAndBoard
    ):
        tracker, board = back_tracker
        cell_1_coordinates = (1, 1)
        cell_1_value = 1
        cell_2_coordinates = (2, 2)
        cell_2_value = 2
        tracker.track_solved_cell(*cell_1_coordinates, cell_1_value)
        tracker.track_solved_cell(*cell_2_coordinates, cell_2_value)
        tracker.rollback()  # Now cell 2 is empty but contains some used values.
        expected_cell_2_used_values: set[int] = set()

        tracker.rollback()

        actual_cell_2_used_values = tracker.get_used_cell_values(*cell_2_coordinates)
        assert expected_cell_2_used_values == actual_cell_2_used_values

    def test_given_cell_with_multiple_tracked_values_when_getting_list_if_used_values_then_all_tracked_values_must_bne_in_the_list(
        self, back_tracker: TrackerAndBoard
    ):
        tracker, _ = back_tracker
        cell_coordinates = (1, 2)
        tracked_value_1 = 8
        tracked_value_2 = 9
        expected_used_values = {tracked_value_1, tracked_value_2}
        tracker.track_solved_cell(*cell_coordinates, tracked_value_1)
        tracker.rollback()  # Rollback value 1. It must still remain in the used values list.
        tracker.track_solved_cell(*cell_coordinates, tracked_value_2)
        tracker.rollback()  # Rollback value 2. It must still remain in the used values list.

        actual_used_values = tracker.get_used_cell_values(*cell_coordinates)

        assert expected_used_values == actual_used_values

    def test_given_tacker_with_no_tracked_values_when_calling_rollback_then_error_must_be_raised(
        self, back_tracker: TrackerAndBoard
    ):
        tracker, _ = back_tracker

        with pytest.raises(NothingToRollbackError):
            tracker.rollback()

    def test_given_tracker_with_1_tracked_cell_when_calling_rollback_more_than_once_then_error_raised(
        self, back_tracker: TrackerAndBoard
    ):
        tracker, _ = back_tracker
        cell_1_coordinates = (1, 1)
        cell_1_value = 1
        tracker.track_solved_cell(*cell_1_coordinates, cell_1_value)
        tracker.rollback()

        with pytest.raises(NothingToRollbackError):
            tracker.rollback()


class TestBackTrackingSolver:
    @classmethod
    def _create_solver(cls, board: Board) -> BackTrackingSolver:
        solver_instance = BackTrackingSolver.create(board)
        return solver_instance

    def test_given_board_when_passing_it_to_backtracking_solver_then_solver_can_be_created_without_errors(
        self, test_board: Board
    ):
        self._create_solver(test_board)

    @pytest.mark.parametrize("row, col", [(row, col) for row in range(BOARD_HEIGHT) for col in range(BOARD_WIDTH)])
    def test_given_empty_board_when_getting_cell_possible_values_then_entire_set_of_valid_values_is_returned(
        self, row, col, empty_test_board: Board
    ):
        solver_obj = self._create_solver(empty_test_board)

        assert list(VALID_VALUES) == solver_obj.get_cell_possible_values(row, col)

    def test_given_non_empty_board_when_asking_solver_for_valid_cell_values_then_it_returns_values_not_used_in_row_col_and_square(
        self, test_board: Board
    ):
        expected_valid_cell_values = [3]
        empty_cell_coordinates = (6, 1)
        solver_obj = self._create_solver(test_board)

        assert expected_valid_cell_values == solver_obj.get_cell_possible_values(*empty_cell_coordinates)

    def test_solve_solvable_sudoku(self, test_board: Board):
        solver_instance = self._create_solver(test_board)

        solver_instance.solve()
        solver_instance.validate_board()
        assert test_board.is_solved()

    def test_solver_can_solve_empty_board(self, empty_test_board: Board):
        backtrack_solver = self._create_solver(empty_test_board)

        backtrack_solver.solve()
        backtrack_solver.validate_board()
        assert empty_test_board.is_solved()
