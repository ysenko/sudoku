import pytest

from sudoku import board

from .fixtures.board import empty_test_board, test_board, NOT_EMPTY_BOARD_EXPECTED_VALUES
from .fixtures.cell import empty_cell, frozen_cell, not_empty_cell, EXPECTED_CELL_VALUE
from .utils.board import get_values_for_square, get_values_for_row
from .utils.cell import create_cell_from_value
from .utils.coordinates import (
    get_all_valid_row_col_combinations,
    get_square_id_and_related_rows_and_cols,
    get_all_squares_top_left_corners,
)


INVALID_CELL_VALUE_ERR_REASON = f"Value must be in {board.VALID_VALUES.union({board.EMPTY_VALUE})}"
INVALID_CELL_VALUE_TYPE_ERR_REASON = "Cell value must be integer"

INVALID_COORDINATES_TYPE_ERR_REASON = "Coordinates must be integers"
INVALID_ROW_IDX_ERR_REASON = f"Row must be between 0 and {board.BOARD_HEIGHT - 1}"
INVALID_COL_IDX_ERR_REASON = f"Col must be between 0 and {board.BOARD_WIDTH - 1}"


class TestCell:
    def test_give_frozen_cell_when_setting_new_value_then_error_must_be_raised(self, frozen_cell: board.Cell):
        desired_value = EXPECTED_CELL_VALUE - 1
        with pytest.raises(board.FrozenCellError) as wrapped_err:
            frozen_cell.set_value(desired_value)

        assert EXPECTED_CELL_VALUE == frozen_cell.get_value()

    def test_given_not_empty_cell_when_setting_new_value_then_new_value_must_be_returned_upon_read(
        self, not_empty_cell: board.Cell
    ):
        desired_cell_value = EXPECTED_CELL_VALUE - 1

        not_empty_cell.set_value(desired_cell_value)

        assert desired_cell_value == not_empty_cell.get_value()

    def test_given_not_empty_cell_when_freezing_its_value_then_cell_must_be_reported_as_frozen(
        self, not_empty_cell: board.Cell
    ):
        not_empty_cell.freeze()

        assert not_empty_cell.is_frozen()
        assert EXPECTED_CELL_VALUE == not_empty_cell.get_value()

    def test_given_not_empty_cell_when_check_for_emptiness_it_must_be_reported_as_empty(self, empty_cell: board.Cell):
        assert empty_cell.is_empty()

    def test_given_not_empty_cell_when_check_for_emptiness_it_must_be_reported_as_not_empty(
        self, not_empty_cell: board.Cell
    ):
        assert not not_empty_cell.is_empty()

    @pytest.mark.parametrize(
        "value, reason",
        [
            (-1, INVALID_CELL_VALUE_ERR_REASON),
            (10, INVALID_CELL_VALUE_ERR_REASON),
            (None, INVALID_CELL_VALUE_TYPE_ERR_REASON),
            (True, INVALID_CELL_VALUE_TYPE_ERR_REASON),
            (False, INVALID_CELL_VALUE_TYPE_ERR_REASON),
            ("1", INVALID_CELL_VALUE_TYPE_ERR_REASON),
        ],
    )
    def test_given_a_cell_when_set_invalid_value_then_correct_error_must_be_raised(
        self, empty_cell: board.Cell, value, reason
    ):
        with pytest.raises(board.InvalidCellValueError) as err_proxy:
            empty_cell.set_value(value)

        err_reason = err_proxy.value.reason
        err_value = err_proxy.value.error_value

        assert value == err_value
        assert reason == err_reason

    @pytest.mark.parametrize(
        "cell, is_empty, description",
        [
            (create_cell_from_value(4), False, "Cell must not be empty"),
            (create_cell_from_value(board.EMPTY_VALUE), True, "Cell must be empty"),
        ],
    )
    def test_given_cell_when_checking_for_emptiness_then_it_must_be_reported_correctly(
        self, cell: board.Cell, is_empty: bool, description: str
    ):
        assert is_empty == cell.is_empty(), description

    def test_given_non_frozen_non_empty_cell_when_clearing_its_value_then_cell_reported_as_empty(self):
        initial_value = 4
        cell = create_cell_from_value(initial_value, frozen=False)

        cell.clear_value()

        assert cell.is_empty()

    def test_given_frozen_cell_when_clearing_its_value_then_it_raises_error(self):
        value = 4
        frozen_cell = create_cell_from_value(value, frozen=True)

        with pytest.raises(board.FrozenCellError):
            frozen_cell.clear_value()


class TestCoordinates:

    coordinates = board.Coordinates()

    @pytest.mark.parametrize("row, col", get_all_valid_row_col_combinations())
    def test_given_valid_row_and_col_when_validating_coordinates_it_must_not_raise_error(self, row: int, col: int):
        self.coordinates.validate_row_and_col(row, col)

    @pytest.mark.parametrize(
        "row, col, reason",
        [
            (True, 1, INVALID_COORDINATES_TYPE_ERR_REASON),
            (1, False, INVALID_COORDINATES_TYPE_ERR_REASON),
            (0.5, 1, INVALID_COORDINATES_TYPE_ERR_REASON),
            ("Hello", 1, INVALID_COORDINATES_TYPE_ERR_REASON),
            (board.BOARD_HEIGHT + 1, 1, INVALID_ROW_IDX_ERR_REASON),
            (-1, 1, INVALID_ROW_IDX_ERR_REASON),
            (1, board.BOARD_WIDTH + 1, INVALID_COL_IDX_ERR_REASON),
            (1, -1, INVALID_COL_IDX_ERR_REASON),
        ],
    )
    def test_given_invalid_row_or_column_when_validating_coordinates_it_must_raise_correct_error(
        self, row: int, col: int, reason: str
    ):
        with pytest.raises(board.InvalidCellAddressError) as err_proxy:
            self.coordinates.validate_row_and_col(row, col)

        err_col = err_proxy.value.col
        err_row = err_proxy.value.row
        err_reason = err_proxy.value.reason

        assert col == err_col
        assert row == err_row
        assert reason == err_reason

    @pytest.mark.parametrize(
        "row, col, expected_square_id",
        get_square_id_and_related_rows_and_cols(square_id=0, top_left_row=0, top_left_col=0)
        + get_square_id_and_related_rows_and_cols(square_id=1, top_left_row=0, top_left_col=3)
        + get_square_id_and_related_rows_and_cols(square_id=2, top_left_row=0, top_left_col=6)
        + get_square_id_and_related_rows_and_cols(square_id=3, top_left_row=3, top_left_col=0)
        + get_square_id_and_related_rows_and_cols(square_id=4, top_left_row=3, top_left_col=3)
        + get_square_id_and_related_rows_and_cols(square_id=5, top_left_row=3, top_left_col=6)
        + get_square_id_and_related_rows_and_cols(square_id=6, top_left_row=6, top_left_col=0)
        + get_square_id_and_related_rows_and_cols(square_id=7, top_left_row=6, top_left_col=3)
        + get_square_id_and_related_rows_and_cols(square_id=8, top_left_row=6, top_left_col=6),
    )
    def test_give_row_col_when_getting_square_id_then_corret_id_must_be_returned(
        self, row: int, col: int, expected_square_id: int
    ):
        actual_square_id = self.coordinates.get_square_id_from_row_col(row, col)

        assert expected_square_id == actual_square_id

    @pytest.mark.parametrize("square_id, expected_row, expected_col", get_all_squares_top_left_corners())
    def test_given_square_id_when_getting_its_top_left_corner_it_returns_correct_result(
        self, square_id: int, expected_row: int, expected_col: int
    ):
        actual_row, actual_col = self.coordinates.get_square_top_left_corner(square_id)

        assert expected_row == actual_row
        assert expected_col == actual_col


class TestBoard:
    def test_given_no_board_when_creating_empty_board_it_returns_board_with_all_cells_empty(self):
        empty_board = board.Board.create_empty()
        all_cells_empty = all([cell.is_empty() for row_cells in empty_board.rows() for cell in row_cells])

        assert all_cells_empty

    def test_given_no_board_when_creating_empty_board_it_returns_board_with_no_frozen_cells(self):
        empty_board = board.Board.create_empty()
        all_cells_frozen = all([not cell.is_frozen() for row_cells in empty_board.rows() for cell in row_cells])

        assert all_cells_frozen

    def test_given_cell_values_when_creating_board_from_them_it_must_return_board_with_correct_cell_values(self):
        test_board = board.Board.create_from_cell_values(NOT_EMPTY_BOARD_EXPECTED_VALUES)

        for row, row_cells in enumerate(test_board.rows()):
            expected_cells = [
                create_cell_from_value(value) for value in get_values_for_row(row, NOT_EMPTY_BOARD_EXPECTED_VALUES)
            ]

            assert expected_cells == row_cells

    def test_given_cell_values_when_creating_board_from_them_then_all_non_empty_cells_must_be_frozen(self):
        test_board = board.Board.create_from_cell_values(NOT_EMPTY_BOARD_EXPECTED_VALUES)

        for row_cells in test_board.rows():
            for cell in row_cells:
                if not cell.is_empty():
                    assert cell.is_frozen()

    def test_given_non_empty_test_board_when_getting_a_board_cell_then_it_must_return_it(self, test_board: board.Board):
        (row, col) = (3, 2)
        expected_value = NOT_EMPTY_BOARD_EXPECTED_VALUES[row][col]

        cell = test_board.get_cell(row, col)

        assert expected_value == cell.get_value()

    def test_given_board_when_iterating_through_its_rows_then_it_returns_them_in_correct_order_with_correct_cells(
        self, test_board: board.Board
    ):
        expected_rows = [[create_cell_from_value(value) for value in row] for row in NOT_EMPTY_BOARD_EXPECTED_VALUES]

        actual_rows = list(test_board.rows())

        assert expected_rows == actual_rows

    def test_given_board_when_iterating_through_its_cols_then_it_returns_correct_cols(self, test_board: board.Board):
        expected_cols = [
            [create_cell_from_value(value) for value in col] for col in zip(*NOT_EMPTY_BOARD_EXPECTED_VALUES)
        ]

        actual_cols = list(test_board.cols())

        assert expected_cols == actual_cols

    def test_given_board_when_iterating_through_its_squares_then_it_returns_them_in_correct_order_with_correct_cells(
        self, test_board: board.Board
    ):
        expected_squares = [
            [create_cell_from_value(value) for value in get_values_for_square(square, NOT_EMPTY_BOARD_EXPECTED_VALUES)]
            for square in range(board.BOARD_SQUARES)
        ]

        actual_squares = list(test_board.squares())

        assert expected_squares == actual_squares
