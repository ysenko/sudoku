from sudoku.formatter import TextFormatter

from .fixtures.board import test_board
from .fixtures.formatter import text_formatter


def test_given_text_formatter_when_serialize_the_board_then_it_returns_correct_str_representation(
    text_formatter: TextFormatter
):
    expected_result = """\
+---+---+---+---+---+---+---+---+---+
|         7 | 4   9   1 | 6       5 |
| 2         |     6     | 3       9 |
|           |         7 |     1     |
+---+---+---+---+---+---+---+---+---+
|     5   8 | 6         |         4 |
|         3 |           |     9     |
|         6 | 2         | 1   8   7 |
+---+---+---+---+---+---+---+---+---+
| 9       4 |     7     |         2 |
| 6   7     | 8   3     |           |
| 8   1     |     4   5 |           |
+---+---+---+---+---+---+---+---+---+\
"""
    assert expected_result == text_formatter.format()


def test_given_text_formatter_when_called_twice_then_produces_same_result_every_time(text_formatter: TextFormatter):
    str_1 = text_formatter.format()
    str_2 = text_formatter.format()

    assert str_1 == str_2
