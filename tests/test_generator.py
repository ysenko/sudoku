import pytest

from sudoku.board import Board
from sudoku.generator import Generator, Complexity
from sudoku.solver import BackTrackingSolver


@pytest.mark.parametrize("complexity", [Complexity.SIMPLE, Complexity.NORMAL, Complexity.HARD])
def test_given_complexity_when_generating_a_board_then_generator_can_generate_solvable_board(complexity):
    gen = Generator(complexity)
    board: Board = gen.generate()

    assert not board.is_solved()

    solver = BackTrackingSolver.create(board)
    solver.validate_board()
    solver.solve()

    assert board.is_solved()
