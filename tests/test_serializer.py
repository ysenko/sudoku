from sudoku.board import Board
from sudoku.serializer import JsonSerializer

from .fixtures.board import test_board, NOT_EMPTY_BOARD_EXPECTED_VALUES
from .utils.serializer import create_board_json_from_values, assert_boards_equal


def test_given_board_when_serializing_it_to_json_then_it_produces_correct_json(test_board: Board):
    serializer = JsonSerializer()
    expected_json_str = create_board_json_from_values(NOT_EMPTY_BOARD_EXPECTED_VALUES)

    actual_json_str = serializer.encode(test_board)

    assert expected_json_str == actual_json_str


def test_given_board_json_when_decoding_it_then_it_returns_correct_board_object(test_board: Board):
    json_to_decode = create_board_json_from_values(NOT_EMPTY_BOARD_EXPECTED_VALUES)
    expected_board = test_board
    serializer = JsonSerializer()

    decoded_board = serializer.decode(json_to_decode)

    assert_boards_equal(expected_board, decoded_board)
