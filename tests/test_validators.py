import pytest

from sudoku.board import Board
from sudoku.errors import ValidationError
from sudoku.validator import ColValidator, RowValidator, SquareValidator, ValidatorProtocol, ValidatorType

from .fixtures.board import empty_test_board, test_board


class ValidatorHelperMixin:
    board: Board
    validator: ValidatorProtocol
    validator_cls = None

    def setup_method(self, method):
        self.board = Board.create_empty()
        self.validator = self.validator_cls(self.board)

    def set_cell_value(self, row: int, col: int, value: int, freeze=False):
        cell = self.board.get_cell(row, col)
        cell.set_value(value)
        if freeze:
            cell.freeze()


class TestRowValidator(ValidatorHelperMixin):

    validator_cls = RowValidator

    def test_given_valid_unsolved_board_when_validating_rows_then_id_does_not_raise_error(self, test_board: Board):
        self.validator.validate()

    def test_given_row_with_duplicated_value_when_running_validation_then_it_raises_error(
        self, empty_test_board: Board
    ):
        first_cell_coordinates = (8, 1)
        duplicated_cell_coordinates = (8, 8)
        duplicated_value = 5

        self.set_cell_value(*first_cell_coordinates, duplicated_value)
        self.set_cell_value(*duplicated_cell_coordinates, duplicated_value)

        with pytest.raises(ValidationError) as err_proxy:
            self.validator.validate()

    def test_given_validation_error_raised_when_checking_error_cells_then_they_contain_row_which_caused_error(
        self, empty_test_board: Board
    ):
        row_with_duplicates = 8
        first_cell_coordinates = (row_with_duplicates, 1)
        duplicated_cell_coordinates = (row_with_duplicates, 8)
        duplicated_value = 5
        self.set_cell_value(*first_cell_coordinates, duplicated_value)
        self.set_cell_value(*duplicated_cell_coordinates, duplicated_value)
        expected_cells_with_error = self.board.get_row(row_with_duplicates)

        with pytest.raises(ValidationError) as err_proxy:
            self.validator.validate()

        raised_err = err_proxy.value
        assert ValidatorType.ROW == raised_err.validation_type
        assert expected_cells_with_error == raised_err.cells

    def test_given_empty_board_when_running_row_validator_then_it_does_not_raise_error(self):
        self.validator.validate()


class TestColValidator(ValidatorHelperMixin):

    validator_cls = ColValidator

    def test_given_valid_unsolved_board_when_validating_cols_then_id_does_not_raise_error(self, test_board: Board):
        col_validator = ColValidator(test_board)
        col_validator.validate()

    def test_given_empty_board_when_validating_cols_then_it_does_not_raise_error(self):
        self.validator.validate()

    def test_given_col_with_duplicated_value_when_running_validation_then_it_raises_error(
        self, empty_test_board: Board
    ):
        col_with_errors = 1
        first_cell_coordinates = (1, col_with_errors)
        duplicated_cell_coordinates = (8, col_with_errors)
        duplicated_value = 5

        self.set_cell_value(*first_cell_coordinates, duplicated_value)
        self.set_cell_value(*duplicated_cell_coordinates, duplicated_value)

        with pytest.raises(ValidationError):
            self.validator.validate()

    def test_given_validation_error_raised_when_checking_error_cells_then_they_contain_row_which_caused_error(
        self, empty_test_board: Board
    ):
        col_with_errors = 1
        first_cell_coordinates = (1, col_with_errors)
        duplicated_cell_coordinates = (8, col_with_errors)
        duplicated_value = 5
        expected_cells_with_error = self.board.get_col(col_with_errors)

        self.set_cell_value(*first_cell_coordinates, duplicated_value)
        self.set_cell_value(*duplicated_cell_coordinates, duplicated_value)

        with pytest.raises(ValidationError) as err_container:
            self.validator.validate()

        raised_err = err_container.value
        assert ValidatorType.COL == raised_err.validation_type
        assert expected_cells_with_error == raised_err.cells


class TestSquareValidator(ValidatorHelperMixin):

    validator_cls = SquareValidator

    def test_given_valid_board_when_validating_squares_then_it_does_not_raise_error(self):
        self.validator.validate

    def test_given_empty_board_when_validating_squares_then_it_does_not_raise_error(self, empty_test_board: Board):
        self.validator = self.validator_cls(empty_test_board)

        self.validator.validate()

    def test_given_board_with_duplicated_value_in_a_square_when_validating_squares_it_raises_error(self):
        first_cell_coordinates = (1, 1)
        duplicated_cell_coordinates = (2, 2)
        duplicated_value = 5

        self.set_cell_value(*first_cell_coordinates, duplicated_value)
        self.set_cell_value(*duplicated_cell_coordinates, duplicated_value)

        with pytest.raises(ValidationError):
            self.validator.validate()

    def test_given_failed_validation_when_check_error_details_then_it_is_of_correct_type_and_contains_error_cells(self):
        square_with_errors = 4
        first_cell_coordinates = (3, 3)
        duplicated_cell_coordinates = (4, 4)
        duplicated_value = 5

        self.set_cell_value(*first_cell_coordinates, duplicated_value)
        self.set_cell_value(*duplicated_cell_coordinates, duplicated_value)

        with pytest.raises(ValidationError) as err_container:
            self.validator.validate()

        error = err_container.value

        assert ValidatorType.SQUARE == error.validation_type
        assert self.board.get_square(square_with_errors) == error.cells
