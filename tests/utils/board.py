from sudoku.board import ListOfCellValues

from .coordinates import get_all_squares_top_left_corners, get_square_id_and_related_rows_and_cols


def get_values_for_square(square: int, values: ListOfCellValues) -> list[int]:
    # TODO: this is ugly. Think of better way to ge all cell coordinates if the given square.
    square_cell_coordinates = None

    for square_id, top_left_row, top_left_col in get_all_squares_top_left_corners():
        if square_id == square:
            square_cell_coordinates = get_square_id_and_related_rows_and_cols(square_id, top_left_row, top_left_col)
            break

    return [values[row][col] for row, col, _ in square_cell_coordinates]


def get_values_for_row(row: int, values: ListOfCellValues) -> list[int]:
    return values[row]
