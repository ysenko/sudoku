from typing import Optional
from sudoku.board import Cell, EMPTY_VALUE


def create_cell_from_value(value: int, frozen: Optional[bool] = None) -> Cell:
    frozen = frozen if frozen is not None else value != EMPTY_VALUE
    return Cell(value=value, frozen=frozen)
