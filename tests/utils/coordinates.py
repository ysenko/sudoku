from itertools import product

from sudoku.board import BOARD_HEIGHT, BOARD_WIDTH, SQUARE_SIZE


SQUARE_TOP_LEFT_POSTITIONS = ((0, 0), (0, 3), (0, 6), (3, 0), (3, 3), (3, 6), (6, 0), (6, 3), (6, 6))


def get_all_valid_row_col_combinations() -> list[tuple[int, int]]:
    valid_rows = range(BOARD_HEIGHT)
    valid_cols = range(BOARD_WIDTH)
    return list(product(valid_rows, valid_cols))


def get_square_id_and_related_rows_and_cols(
    square_id: int, top_left_row: int, top_left_col: int
) -> list[tuple[int, int, int]]:
    """Helper utility to generate square id and all related row and col combinations.

    Args:
        square_id (int): id of the square to get
        top_left_row (int): top left row of the square
        top_left_col (int): top left col of the square

    Returns:
        list[tuple[int, int, int]]: list of tuples where each element represents (row, col, square_id)
    """
    return [
        (top_left_row + row_delta, top_left_col + col_delta, square_id)
        for row_delta in range(SQUARE_SIZE)
        for col_delta in range(SQUARE_SIZE)
    ]


def get_all_squares_top_left_corners() -> list[tuple[int, int, int]]:
    """Returns top left corners for all squares.

    Returns:
        list[tuple[int, int, int]]: list of tuples where each tuple represents a Square. The first element of each tuple is a square_id, second and third elements are row and col respectively.
    """
    return [(square_id, row, col) for square_id, (row, col) in enumerate(SQUARE_TOP_LEFT_POSTITIONS)]
