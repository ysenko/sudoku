import json

from sudoku.board import EMPTY_VALUE, Board


def create_board_json_from_values(values: list[list[int]]) -> str:
    return json.dumps(
        {"board": {"cells": [[{"value": value, "frozen": value != EMPTY_VALUE} for value in row] for row in values]}}
    )


def assert_boards_equal(b1: Board, b2: Board):
    for row_idx, b1_row in enumerate(b1.rows()):
        b2_row = b2.get_row(row_idx)
        assert b1_row == b2_row
